### Librerías usadas
- [nodemon](https://www.npmjs.com/package/nodemon) para hot reload del server   
- [rate-limiter-flexible](https://github.com/animir/node-rate-limiter-flexible) para evitar ataques de brute force y escaneos agresivos ([ejemplos de uso](https://github.com/animir/node-rate-limiter-flexible/wiki/Overall-example))
- [helmet](https://www.npmjs.com/package/helmet) para settear algunos headers de seguridad
- [cors](https://expressjs.com/en/resources/middleware/cors.html) para CORS
- `morgan` para logear las requests   
- `jsonwebtoken` para crear/validar tokens JWT   
- `bcrypt` para hashear las contraseñas   
- [sequelize](https://sequelize.org/master/) ORM de la base de datos
- `sqlite3` para poder usar la librería anterior con SQLite3
- `chalk` para console.logear con colores
- `cookie-parser` para almacenar de manera segura en el cliente los tokens de JWT
- [compression](https://www.npmjs.com/package/compression) para comprimir las responses para mayor performance
([comparativa de almacenamientos](https://stormpath.com/blog/where-to-store-your-jwts-cookies-vs-html5-web-storage))

### Probando el proyecto
Creá un archivo llamado `.env` basándote en `.env.example`:

`cp .env.example .env`

Levantá el servidor:

`npm run serve`

Logineate (estos parámetros están en `.env`):

`curl localhost:3000/user/login -d'username=usu&password=123'`

Copiá el token JWT devuelto y usalo para probar la url privada (debería devolver toda tu data):

`curl localhost:3000/user/private -H 'authorization: Bearer EL_TOKEN_ANTERIOR'`

Otra forma sin tener que copiar el token a mano es usando un archivo intermedio:

```
curl localhost:3000/login -d'username=usu&password=123' > token.jwt
curl localhost:3000/private -H "authorization: Bearer `cat token.jwt`"
```

### Docker
Para correr el proyecto con docker buildear la imagen:
`docker build -t express-first .`

Y correrla pasándole el `.env` y abriendo el puerto 3000:
`docker run --rm --init -p3000:3000 --env-file=.env --name express-first express-first`

La opción `--rm` hace que después de parar el container se borre todo su contenido.
La opción `--init` hace que ande `SIGINT`/`CTRL-C` (sacado de las [Best Practices](https://github.com/nodejs/docker-node#best-practices)).

Para soporte del cliente del front agregar el volumen correspondiente como opción:
`-v"$(pwd)/client:/home/app/client"`
