// basado en https://github.com/IHIutch/heroku-nuxt/blob/ea7dcc94371620e502b5d7586a2c409d5babf5ca/api/v1/models/Answer.js
const {Model} = require("sequelize");
const bcrypt = require("bcrypt");

const init = (sequelize, DataTypes) =>
  User.init(
    {
      // DataTypes - https://sequelize.org/master/manual/model-basics.html#data-types
      username: DataTypes.STRING,
      passwordHash: DataTypes.STRING,
      googleId: DataTypes.INTEGER,
      displayName: DataTypes.STRING
    },
    {
      sequelize,
      modelName: "User"
    }
  );

class User extends Model {
  static getById(userId) {
    return User.findOne({
      where: {
        id: userId
      }
    });
  }

  static async localLoginUser(username, password) {
    const user = await User.findOne({
      where: {
        username
      }
    });

    if (!user) return false;

    const isValid = await bcrypt.compare(password, user.passwordHash);

    return isValid && user;
  }

  static async getOrCreateGoogleUser(googleId, displayName) {
    let user = await User.findOne({
      where: {
        googleId
      }
    });

    if (!user)
      user = await User.create({
        displayName,
        googleId
      });

    return user;
  }
}

module.exports = {
  init
};
