const chalk = require("chalk");
const {Sequelize, Model, DataTypes} = require("sequelize");
const bcrypt = require("bcrypt");

const config = require("../config");
const User = require("./user");

const modelsList = [User];

// objeto a exportar para usar en el código
const db = {};

// basado en https://github.com/IHIutch/heroku-nuxt/blob/ea7dcc94371620e502b5d7586a2c409d5babf5ca/api/v1/models/index.js
db.init = async () => {
  // configuración de conexión
  //const sequelize = new Sequelize('sqlite::memory:');
  const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: config.DB_FILENAME,
    logging: config.DEBUG_SQL && console.log
  });
  db.sequelize = sequelize;

  // inicializamos modelos
  const models = await Promise.all(
    modelsList.map(model => model.init(sequelize, Sequelize.DataTypes))
  );

  // hacemos asociaciones (si existen) y cargamos modelos en objeto db
  models.forEach(model => {
    if (model.associate) model.associate(db);
    db[model.name] = model;
  });

  // probamos conexión
  // https://sequelize.org/master/class/lib/sequelize.js~Sequelize.html#instance-method-authenticate
  await sequelize.authenticate();
  console.log(chalk.green("Conexión a base de datos exitosa"));

  // crear las tablas si no existen
  // https://sequelize.org/master/manual/model-basics.html#model-synchronization
  await sequelize.sync();

  if (config.DEV_MODE) {
    // si no hay ningún usuarie cargado cargar el de testeo
    const users = await db.User.count();
    if (!users) {
      // cargar usuarie de testeo
      const passwordHash = await bcrypt.hash(
        config.TEST_PASSWORD,
        parseInt(config.BCRYPT_SALT_ROUNDS)
      );

      const user = await db.User.create({
        username: config.TEST_USERNAME,
        passwordHash
      });

      console.log(
        chalk.green(
          `Usuarix de testeo creadx: ${config.TEST_USERNAME}/${config.TEST_PASSWORD}`
        )
      );
    }
  }
};

module.exports = db;
