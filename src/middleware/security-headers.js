const helmet = require("helmet");
const cors = require("cors");

const config = require("../config");
const rateLimiterMiddleware = require("./rate-limiter");

module.exports = app => {
  // limitamos cantidad de requests seguidas por IP
  app.use(rateLimiterMiddleware);

  // helmet ayuda a securizar un poco (settea varios headers)
  const cspDefaultDirectives = helmet.contentSecurityPolicy.getDefaultDirectives();

  if (!config.BEHIND_SSL_PROXY && !config.SERVER_ENABLE_HTTPS)
    delete cspDefaultDirectives["upgrade-insecure-requests"];

  app.use(
    helmet({
      contentSecurityPolicy: {
        directives: {
          ...cspDefaultDirectives,
          // por ahora no usamos frames
          "frame-ancestors": ["'none'"],
          // recomendado por https://csp-evaluator.withgoogle.com/
          "require-trusted-types-for": ["'script'"]
        }
      },
      // por ahora no usamos frames (para browsers viejos)
      frameguard: {action: "deny"}
    })
  );
  // ...is equivalent to this:
  /*// este feature es el más importante! configurarlo con conocimiento!
  // sets the Content-Security-Policy header which helps mitigate cross-site scripting attacks, among other things
  // default-src, base-uri, block-all-mixed-content, font-src, frame-ancestors, etc.
  app.use(helmet.contentSecurityPolicy());
  // sets the X-DNS-Prefetch-Control header to off, which can improve user privacy
  app.use(helmet.dnsPrefetchControl());
  // sets the Expect-CT header which helps mitigate misissued SSL certificates
  // The Expect-CT will likely become obsolete in June 2021.
  app.use(helmet.expectCt());
  // sets the X-Frame-Options header to SAMEORIGIN, useful on old browsers
  app.use(helmet.frameguard());
  // removes the X-Powered-By header
  app.use(helmet.hidePoweredBy());
  // sets the Strict-Transport-Security header which tells browsers to prefer HTTPS over insecure HTTP
  app.use(helmet.hsts());
  // sets the X-Download-Options header, which is specific to Internet Explorer 8.
  // It forces potentially-unsafe downloads to be saved, mitigating execution of HTML in your site's context
  app.use(helmet.ieNoOpen());
  // sets the X-Content-Type-Options header to nosniff. This mitigates MIME type sniffing
  app.use(helmet.noSniff());
  // sets the X-Permitted-Cross-Domain-Policies header to none, which tells some clients (mostly Adobe Flash
  // Player/Acrobat, Microsoft Silverlight) your domain's policy for loading cross-domain content
  app.use(helmet.permittedCrossDomainPolicies());
  // sets the Referrer-Policy header to no-referrer to prevent tracking or leaking information
  app.use(helmet.referrerPolicy());
  // sets the X-XSS-Protection header to 0. X-XSS-Protection is dead and buggy.
  app.use(helmet.xssFilter());*/

  const corsOptions = {
    // para poder usar cookies para credenciales
    credentials: true,
    // some legacy browsers (IE11, various SmartTVs) choke on 204 (default)
    optionsSuccessStatus: 200
  };

  // en dev tenemos el dev server de react en otra url
  if (config.FRONTEND_URL)
    Object.assign(corsOptions, {origin: config.FRONTEND_URL});

  app.use(cors(corsOptions));
};
