const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const {promisify} = require("util");

const db = require("../../db");
const {restrict} = require("../../auth");

module.exports = app => {
  passport.use(
    new LocalStrategy(async (username, password, done) => {
      let user;
      try {
        user = await db.User.localLoginUser(username, password);
      } catch (err) {
        return done(err);
      }
      if (user) return done(null, user);
      else return done(null, false, {message: "Login inválido"});
    })
  );

  app.post("/login", passport.authenticate("local"), (req, res) => {
    // When authentication was successful this function get called.
    // `req.user` contains the authenticated user.
    res.sendStatus(req.user ? 200 : 404);
  });

  app.get("/logout", restrict, async (req, res, next) => {
    if (req.session) {
      // For Delete Session Object
      try {
        const adestroy = promisify(req.session.destroy).bind(req.session);
        await adestroy();
      } catch (err) {
        return next(err);
      }
    }
    res.sendStatus(200);
  });

  app.get("/restore-session", restrict, async (req, res) =>
    res.sendStatus(200)
  );
};
