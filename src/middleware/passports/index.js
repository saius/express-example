const passport = require("passport");
const localLogin = require("./local-login");
const googleLogin = require("./google-login");

const config = require("../../config");
const db = require("../../db");

module.exports = app => {
  app.use(passport.initialize());
  // sets Passport as a middleware between the cookie that Express-session sets and
  // Passport’s own user authentication checks
  app.use(passport.session());

  passport.serializeUser((user, cb) => cb(null, user.id));

  passport.deserializeUser(async (id, cb) => {
    const user = await db.User.getById(id);
    cb(null, user);
  });

  localLogin(app);

  if (config.GOOGLE_CLIENT_ID && config.GOOGLE_CLIENT_SECRET) googleLogin(app);
};
