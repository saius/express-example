// passportjs guide - http://www.passportjs.org/docs/google/
// Integrating Google Sign-In into your web app - https://developers.google.com/identity/sign-in/web/sign-in
// link a las credentials de google: https://console.developers.google.com/apis/credentials
const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;

const config = require("../../config");
const db = require("../../db");

module.exports = app => {
  // Use the GoogleStrategy within Passport.
  //   Strategies in Passport require a `verify` function, which accept
  //   credentials (in this case, an accessToken, refreshToken, and Google
  //   profile), and invoke a callback with a user object.
  passport.use(
    new GoogleStrategy(
      {
        clientID: config.GOOGLE_CLIENT_ID,
        clientSecret: config.GOOGLE_CLIENT_SECRET,
        callbackURL: "http://localhost:3000/auth/google/callback"
      },
      async (accessToken, refreshToken, profile, done) => {
        // profile = {id, displayName, name.givenName, emails[0]: {value, verified}, photos[0].value}
        const user = await db.User.getOrCreateGoogleUser(
          profile.id,
          profile.displayName
        );
        return done(null, user);
      }
    )
  );

  // GET /auth/google
  //   Use passport.authenticate() as route middleware to authenticate the
  //   request.  The first step in Google authentication will involve
  //   redirecting the user to google.com.  After authorization, Google
  //   will redirect the user back to this application at /auth/google/callback
  app.get(
    "/auth/google",
    passport.authenticate("google", {
      //scope: ["https://www.googleapis.com/auth/plus.login"]
      scope: ["email", "profile"]
    })
  );

  // GET /auth/google/callback
  //   Use passport.authenticate() as route middleware to authenticate the
  //   request.  If authentication fails, the user will be redirected back to the
  //   login page.  Otherwise, the primary route function function will be called,
  //   which, in this example, will redirect the user to the home page.
  app.get(
    "/auth/google/callback",
    passport.authenticate("google", {failureRedirect: "/"}),
    function(req, res) {
      res.redirect("/");
    }
  );
};
