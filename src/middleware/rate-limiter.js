// código basado en https://github.com/animir/node-rate-limiter-flexible/wiki/Express-Middleware
// y https://github.com/animir/node-rate-limiter-flexible/wiki/Memory
const {RateLimiterMemory} = require("rate-limiter-flexible");

const rateLimiter = new RateLimiterMemory({
  keyPrefix: "middleware",
  points: 20, // 20 requests
  duration: 2, // en 2 segundo (por IP)
  blockDuration: 15 // bloquea por 15 segundos
});

const rateLimiterMiddleware = (req, res, next) => {
  rateLimiter
    .consume(req.ip)
    .then(() => {
      next();
    })
    .catch(() => {
      res.status(429).send("Too Many Requests");
    });
};

module.exports = rateLimiterMiddleware;
