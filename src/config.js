const dotenv = require("dotenv");
const chalk = require("chalk");

// cargamos .env
dotenv.config();

parseFlag = value =>
  value && ["true", "y", "yes", "1"].includes(value.toLowerCase());

const config = {
  NODE_ENV: process.env.NODE_ENV,
  DEV_MODE: process.env.NODE_ENV == "development",

  SERVER_PORT: parseInt(process.env.SERVER_PORT) || 3000,

  BEHIND_SSL_PROXY: parseFlag(process.env.BEHIND_SSL_PROXY) || false,
  SERVER_ENABLE_HTTPS: parseFlag(process.env.SERVER_ENABLE_HTTPS) || false,
  HTTPS_KEY_PATH: process.env.HTTPS_KEY_PATH,
  HTTPS_CERT_PATH: process.env.HTTPS_CERT_PATH,

  SESSION_SECRET: process.env.SESSION_SECRET,

  FRONTEND_SERVE_STATIC: parseFlag(process.env.FRONTEND_SERVE_STATIC) || true,
  FRONTEND_URL: process.env.FRONTEND_URL,

  DB_FILENAME: process.env.DB_FILENAME || "database.sqlite",
  DEBUG_SQL: parseFlag(process.env.DEBUG_SQL) || false,

  BCRYPT_SALT_ROUNDS: parseInt(process.env.BCRYPT_SALT_ROUNDS) || 12,

  TEST_USERNAME: process.env.TEST_USERNAME || "usu",
  TEST_PASSWORD: process.env.TEST_PASSWORD || "123",

  GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
  GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET
};

if (config.FRONTEND_SERVE_STATIC && config.FRONTEND_URL) {
  console.error(
    chalk.red(
      "Las variables de configuración FRONTEND_SERVE_STATIC y FRONTEND_URL no son compatibles juntas. Ignorando FRONTEND_URL."
    )
  );
  config.FRONTEND_URL = undefined;
}

if (
  config.SERVER_ENABLE_HTTPS &&
  !(config.HTTPS_KEY_PATH && config.HTTPS_CERT_PATH)
) {
  console.error(
    chalk.red(
      "Debe configurar HTTPS_KEY_PATH y HTTPS_CERT_PATH para habilitar HTTPS. Deshabilitando HTTPS."
    )
  );
  config.SERVER_ENABLE_HTTPS = false;
}

module.exports = config;
