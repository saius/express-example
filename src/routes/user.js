const express = require("express");

const {restrict} = require("../auth");
const db = require("../db");

const router = express.Router();

router.get("/", async (req, res) => {
  let users = await db.User.findAll();
  res.json(users.map(user => user.username));
});

router.get("/private", restrict, async (req, res) => {
  res.send(req.user);
});

module.exports = router;
