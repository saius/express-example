const chalk = require("chalk");
const express = require("express");
const morgan = require("morgan");
const compression = require("compression");
const session = require("express-session");

const config = require("./config");
const db = require("./db");
const securityHeaders = require("./middleware/security-headers");
const passports = require("./middleware/passports");

const app = express();

console.log(chalk.green(`Creando app en modo=${config.NODE_ENV}`));

// loggear requests
app.use(morgan(config.DEV_MODE ? "dev" : "combined"));

// unless trust proxy is configured it will incorrectly register the proxy’s IP address as the client IP address
// enabling it uses X-Forwarded-* headers: https://expressjs.com/en/guide/behind-proxies.html
//app.set('trust proxy', <TRUE/IP/SUBNET>)

// cors, csp, ratelimiter, etc
securityHeaders(app);

// comprimir las responses comprimibles
app.use(compression());

// parsear json
app.use(express.json());

// parsear POST (application/x-www-form-urlencoded)
app.use(
  express.urlencoded({
    extended: true
  })
);

app.use(
  session({
    secret: config.SESSION_SECRET,
    resave: true,
    saveUninitialized: false
  })
);

// seteamos logins (local, google, etc.)
passports(app);

// rutas de nuestra app
app.use("/user", require("./routes/user"));

// vinculación con el front
if (config.FRONTEND_SERVE_STATIC) {
  const path = require("path");
  app.use(express.static(path.join(__dirname, "../client")));
  app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, "../client/index.html"));
  });
} else if (config.FRONTEND_URL && config.DEV_MODE)
  app.get("*", (req, res) =>
    res.send(`Corriendo frontend en url aparte, andá a ${config.FRONTEND_URL}`)
  );

// custom error handler - https://expressjs.com/en/guide/error-handling.html
// para llamar con next(err) o Promise.catch(next)
app.use(function(err, req, res, next) {
  console.error(err.stack);

  if (res.headersSent) {
    return next(err);
  } else {
    let status = err.status || 500;
    res.status(status);
    if (config.DEV_MODE) res.send(err.message);
    else res.send("Ha ocurrido un error");
  }
});

// arrancamos el server!
db.init().then(() => {
  if (config.SERVER_ENABLE_HTTPS) {
    // For production systems, you're probably better off using Nginx or HAProxy
    // to proxy requests to your nodejs app (as HTTP internally)
    const fs = require("fs");
    const https = require("https");
    const privateKey = fs.readFileSync(config.HTTPS_KEY_PATH, "utf8");
    const certificate = fs.readFileSync(config.HTTPS_CERT_PATH, "utf8");
    const options = {key: privateKey, cert: certificate};
    https.createServer(options, app).listen(config.SERVER_PORT, () => {
      console.log(
        chalk.green(
          `App HTTPS escuchando conexiones en https://localhost:${config.SERVER_PORT}`
        )
      );
    });
  } else
    app.listen(config.SERVER_PORT, () => {
      console.log(
        chalk.green(
          `App escuchando conexiones en http://localhost:${config.SERVER_PORT}`
        )
      );
    });
});
