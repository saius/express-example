# código basado en https://medium.com/javascript-in-plain-english/build-a-production-ready-node-express-api-with-docker-9a45443427a0
# y https://github.com/alexellis/expressjs-k8s/blob/master/Dockerfile
FROM node:alpine

RUN mkdir -p /home/app

WORKDIR /home/app

COPY package*.json ./

ENV NODE_ENV production

RUN npm ci --only=production

# solo copiar código dentro de src, sino copiaría .git, .env, etc.
COPY src src

RUN addgroup -S app && adduser -S -g app app

RUN chown app:app -R /home/app

USER app

EXPOSE 3000

ENTRYPOINT ["node", "./src/app.js"]
